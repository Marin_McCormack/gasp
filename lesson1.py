from gasp import *
my_name = 'Marin'
print('Hello, ' + my_name + '!')
for x in 1, 2, 3, 4, 5:
    print(x, x * x)

print(str(7) + 'Marin')

for number in 2, 4, 6:
    print(2 * number)

begin_graphics()

Line((100, 100), (200, 200))
Circle((320, 240), 40)
Box((400, 300), 100, 85)
update_when('key_pressed')
end_graphics()
